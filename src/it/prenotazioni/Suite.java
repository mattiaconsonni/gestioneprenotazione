package it.prenotazioni;

public class Suite extends Camera{

    public Suite(int numeroPrenotazione, boolean statoPrenotazione) {
        super(numeroPrenotazione, statoPrenotazione);
    }

    @Override
    public String toString() {
        String risultato =  "La camera suite " + super.getNumeroPrenotazione();
        if (super.getStatoPrenotazione())
            return risultato + " è disponibile.";
        else
            return risultato + " non è disponibile";
    }
}
