package it.prenotazioni;

public abstract class Camera {

    private int numeroPrenotazione;
    private boolean statoPrenotazione;

    public Camera(int numeroPrenotazione, boolean statoPrenotazione) {
        this.numeroPrenotazione  = numeroPrenotazione;
        this.statoPrenotazione = statoPrenotazione;
    }

    public int getNumeroPrenotazione() {
        return this.numeroPrenotazione;
    }

    public void setNumeroPrenotazione(int numeroPrenotazione) {
        this.numeroPrenotazione = numeroPrenotazione;
    }

    public boolean getStatoPrenotazione() {
        return this.statoPrenotazione;
    }

    public void setStatoPrenotazione(boolean statoPrenotazione) {
        this.statoPrenotazione = statoPrenotazione;
    }

    @Override
    public abstract String toString();
}
