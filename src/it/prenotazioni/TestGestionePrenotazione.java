package it.prenotazioni;

public class TestGestionePrenotazione {
    public static void main(String[] args) {
        Camera camera1 = new Singola(55, false);
        Camera camera2 = new Doppia(1, true);
        Camera camera3 = new Singola(32, true);
        Camera camera4 = new Suite(40, false);
        GestorePrenotazioni gestorePrenotazioni = new GestorePrenotazioni();
        gestorePrenotazioni.aggiungiCamera(camera1);
        gestorePrenotazioni.aggiungiCamera(camera2);
        gestorePrenotazioni.aggiungiCamera(camera3);
        gestorePrenotazioni.aggiungiCamera(camera4);
        gestorePrenotazioni.visualizzaCamereDisponibili();
        gestorePrenotazioni.rimuoviCamera(55);
        System.out.println("Aggiornamento: ");
        gestorePrenotazioni.visualizzaCamereDisponibili();
        gestorePrenotazioni.aggiornaStatoPrenotazione(32, false);
        System.out.println("Aggiornamento: ");
        gestorePrenotazioni.visualizzaCamereDisponibili();
    }
}
