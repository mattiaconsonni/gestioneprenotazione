package it.prenotazioni;

public class Doppia extends Camera{

    public Doppia(int numeroPrenotazione, boolean statoPrenotazione) {
        super(numeroPrenotazione, statoPrenotazione);
    }

    @Override
    public String toString() {
        String risultato =  "La camera doppia " + super.getNumeroPrenotazione();
        if (super.getStatoPrenotazione())
            return risultato + " è disponibile.";
        else
            return risultato + " non è disponibile";
    }
}
