package it.prenotazioni;

import java.util.HashMap;
import java.util.Map;

public class GestorePrenotazioni {

    private Map<Integer, Camera> prenotazioni;

    public GestorePrenotazioni() {
        this.prenotazioni = new HashMap<>();
    }

    public void aggiungiCamera(Camera camera) {
        this.prenotazioni.put(camera.getNumeroPrenotazione(), camera);
    }

    public void rimuoviCamera(int numeroCamera) {
        this.prenotazioni.remove(numeroCamera);
    }

    public Camera restituisciCamera(int numeroCamera) {
        if (this.prenotazioni.containsKey(numeroCamera))
            return this.prenotazioni.get(numeroCamera);
        return null;
    }

    public void aggiornaStatoPrenotazione(int numeroCamera, boolean prenotata) {
        if (this.prenotazioni.containsKey(numeroCamera)) {
            Camera camera = this.prenotazioni.get(numeroCamera);
            camera.setStatoPrenotazione(prenotata);
        }
    }

    public void visualizzaCamereDisponibili() {
        for (Map.Entry<Integer, Camera> entry: this.prenotazioni.entrySet()) {
            System.out.println(entry.getValue().toString());
        }
    }
}
