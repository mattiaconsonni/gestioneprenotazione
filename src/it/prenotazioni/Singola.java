package it.prenotazioni;

public class Singola extends Camera{

    public Singola(int numeroPrenotazione, boolean statoPrenotazione) {
        super(numeroPrenotazione, statoPrenotazione);
    }

    @Override
    public String toString() {
        String risultato =  "La camera singola " + super.getNumeroPrenotazione();
        if (super.getStatoPrenotazione())
            return risultato + " è disponibile.";
        else
            return risultato + " non è disponibile";
    }
}
